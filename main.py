def gen_sum(array):
    sum_array = [0]
    for elem in array:
        sum_array.append(sum_array[-1] + elem)
    return sum_array


def gen_map(s):
    m = {}
    for i, elem in enumerate(s):
        m[elem] = i  # index to number
    return m


def fill_map(m, x):
    for i in range(1, x + 1):
        if i not in m:
            m[i] = m[i - 1]
    return m


def get_result(x, lena, lenb, a, b):
    suma = gen_sum(a)
    sumb = gen_sum(b)

    mapa = gen_map(suma)
    mapb = gen_map(sumb)

    mapa = fill_map(mapa, x)
    mapb = fill_map(mapb, x)

    max_sum = 0
    for i in range(x+1):
        s = mapa[i] + mapb[x-i]
        if s > max_sum:
            max_sum = s

    return max_sum


def main():
    with open("input", "r") as in_stream:
        x, lena, lenb = (int(x) for x in in_stream.readline().split(" "))
        a = [int(x) for x in in_stream.readline().replace("\n", "").split(" ")]
        b = [int(x) for x in in_stream.readline().replace("\n", "").split(" ")]
        r = get_result(x, lena, lenb, a, b)
        print(r)


if __name__ == "__main__":
    main()
